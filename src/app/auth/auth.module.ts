import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnsAuthService } from "./auth.service";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [AnsAuthService],
})
export class AnsAuthModule { 
  
 }
