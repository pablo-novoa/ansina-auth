import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AnsAuthModule } from "./auth/auth.module";

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AnsAuthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
